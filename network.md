# Network

## Connectivity
### Check if port is open on remote machine
```
nc -zv [host] [port]

[host] - remote machine host
[port] - remote machine port
```

### Check what ports are in use by apps
```
sudo lsof -i -P -n | grep LISTEN
sudo netstat -tulpn
```

https://www.cyberciti.biz/faq/unix-linux-check-if-port-is-in-use-command/

### Scan open ports on remote machine
```
nmap [host]

[host] - remote machine host
```

### Enumerate SSL ciphers on port and host
```
nmap --script ssl-enum-ciphers -p [port] [host]

[host] - remote machine host
[port] - remote machine port. default will be 443
```

### Check how many NS records your domain has
```
dig ns [domain.name]
```

### Run a quick scan of machines in our network without trying to see if any port is open
https://www.redhat.com/sysadmin/quick-nmap-inventory
```
nmap -sn 192.168.0.0/24
```

### Check your WANT IP
```
curl -s https://hostpapastatus.com/ip/ | grep -ioE "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
curl -s https://www.whatismyip.com/ | grep -ioE "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
```


## Security
## Security - Netfilter

## Security - Iptables

### List iptables
```
sudo iptables -L
```

### List iptables and details info about - TODO: about what 
```
sudo iptables -nvL
```

### Add a rule. Example - open http and https ports
```
sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -j ACCEPT
```