# System

## Storage

### Check occupied space by folders
Shows in human readable format
```
sudo du -sh *
```

Shows in kilos and sorted
```
du -sk * | sort -n
```

Shows with total and sorted
```
du -sch * .[!.]* | sort -rh
```



## Logs and journal

### Services logs
```
journalctl -f -u inlets
journalctl -f -u caddy
```

### Check open files by a process
- grab process id `ps aux | grep process_name`
- find process id in result output
- count open files `sudo ls -l /proc/[proc_id]/fd | wc -l`
- show open files `sudo ls -l /proc/[proc_id]/fd`
- TODO: what is diff?
- count open files `sudo lsof -p [proc_id] | wc -l`
- show open files `sudo lsof -p [proc_id]`